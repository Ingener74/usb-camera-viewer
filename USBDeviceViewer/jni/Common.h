/*
 * Common.h
 *
 *  Created on: Jul 16, 2014
 *      Author: ingener
 */

#ifndef COMMON_H_
#define COMMON_H_

//#include <execinfo.h>

#include <memory>
#include <vector>
#include <future>
#include <string>
#include <thread>
#include <cstring>
#include <sstream>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <functional>

#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>

#include <libusb/libusb.h>
#include <libuvc/libuvc.h>

#endif /* COMMON_H_ */
